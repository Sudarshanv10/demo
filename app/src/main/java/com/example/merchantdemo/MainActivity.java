package com.example.merchantdemo;

import androidx.activity.ComponentActivity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import in.juspay.hypersdk.data.JuspayResponseHandler;
import in.juspay.hypersdk.ui.HyperPaymentsCallbackAdapter;
import in.juspay.services.HyperServices;



public class MainActivity extends AppCompatActivity {
    HyperServices hyperInstance ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        hyperInstance = new HyperServices(this,
                findViewById(R.id.hyperSDKViewGroup));
        
        
        
        /////////////

//////






        JSONObject preFetchPayload = new JSONObject();

        try {
            JSONObject innerPayload = new JSONObject();
            String clientId = "<clientID>";
            innerPayload.put("clientId", clientId);
            preFetchPayload.put("payload", innerPayload);

            preFetchPayload.put("service", "in.juspay.hyperpay");

            boolean useBetaAssets = true; // false
            preFetchPayload.put("betaAssets", useBetaAssets);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HyperServices.preFetch(this, preFetchPayload);

        ///////////


        hyperInstance.initiate(preFetchPayload, new HyperPaymentsCallbackAdapter() {
            @Override
            public void onEvent(JSONObject data, JuspayResponseHandler handler) {
                try {
                    String event = data.getString("event");
                    if (event.equals("show_loader")) {
                        // Show some loader here
                    } else if (event.equals("hide_loader")) {
                        // Hide Loader
                    } else if (event.equals("initiate_result")) {
                        // Get the response
                        JSONObject response = data.optJSONObject("payload");
                    } else if (event.equals("process_result")) {
                        // Get the response
                        JSONObject response = data.optJSONObject("payload");
                        //Merchant handling
                    }
                } catch (Exception e) {
                    // merchant code...
                }
            }
        });

        hyperInstance.process(preFetchPayload);
        onDestroy();





    }
    protected  void onDestroy()
    {
        super.onDestroy();
        hyperInstance.terminate();
    }





}